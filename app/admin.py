from django.contrib import admin
from django.core.checks import messages
from djcelery.models import PeriodicTask, TaskState
from djcelery.admin import PeriodicTaskAdmin, TaskMonitor, colored_state

from app.filter import HandlingFilter, AddressFilter
from app.models import Person, Notification, Company
from app.tasks import load_logo, find_info, search_info


class UserAdmin(admin.ModelAdmin):
    search_fields = ["username", 'email', 'city']

    list_display = ('username', 'email', 'has_photo', 'first_name', 'last_name', 'country', 'city', 'phone',
                    'last_ip', 'ip_addresses', 'created', 'is_staff')


class NotificationAdmin(admin.ModelAdmin):
    list_display = ['message', 'active']


class CompanyAdmin(admin.ModelAdmin):
    class Media:
        js = ('js/jquery-1.12.0.js', "js/addon_admin.js",)
    list_display = ['name', 'location', 'addresses', 'logo', 'link']

    search_fields = ('name', 'location', 'addresses')
    list_filter = (HandlingFilter, AddressFilter,)

    def changelist_view(self, request, extra_context=None):
        try:
            action = self.get_actions(request)[request.POST['action']][0]
            action_acts_on_all = action.acts_on_all
        except (KeyError, AttributeError):
            action_acts_on_all = False

        if action_acts_on_all:
            post = request.POST.copy()
            post.setlist(admin.helpers.ACTION_CHECKBOX_NAME,
                         self.model.objects.values_list('id', flat=True))
            request.POST = post

        return admin.ModelAdmin.changelist_view(self, request, extra_context)

    def sync_logo_company(self, request, queryset):
        load_logo.apply_async()
        self.message_user(request, "Start synchronize logo images of company", messages.INFO)

    sync_logo_company.acts_on_all = True

    def find_info_company(self, request, queryset):
        find_info.apply_async()
        self.message_user(request, "Start finding info of company", messages.INFO)

    def autoupdate_company_info(self, request, queryset):
        for i in queryset:
            search_info(i)

    find_info_company.acts_on_all = True

    actions = ['sync_logo_company', 'find_info_company', 'autoupdate_company_info']


class TaskAdmin(TaskMonitor):
    list_display = ["task_id", colored_state, "name", "runtime", "worker"]

    def delete_selected_task(self, request, queryset):
        queryset.delete()

    actions = ['delete_selected_task']

admin.site.site_header = 'Skyfinder Administration'
admin.site.unregister(PeriodicTask)
admin.site.unregister(TaskState)
admin.site.register(TaskState, TaskAdmin)
admin.site.register(Person, UserAdmin)
admin.site.register(Notification, NotificationAdmin)
admin.site.register(Company, CompanyAdmin)
