# -*- coding: UTF-8 -*-

from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _

from app.models import Company


class HandlingFilter(SimpleListFilter):
    title = 'wait for handling'
    parameter_name = 'handling'

    def lookups(self, request, model_admin):
        yield ('handling', _('wait handling'))

    def queryset(self, request, queryset):
        if self.value() == 'handling':
            return Company.wait_handling()
        else:
            return queryset


class AddressFilter(SimpleListFilter):
    title = 'have address'
    parameter_name = 'without_address'

    def lookups(self, request, model_admin):
        yield ('without_address', _('without address'))

    def queryset(self, request, queryset):
        if self.value() == 'without_address':
            return Company.without_address()
        else:
            return queryset
