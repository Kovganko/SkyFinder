/**
 * Created by n_kovganko on 10.12.2015.
 */

var opts = {
    lines: 15 // The number of lines to draw
    , length: 36 // The length of each line
    , width: 14 // The line thickness
    , radius: 42 // The radius of the inner circle
    , scale: 1.25 // Scales overall size of the spinner
    , corners: 0.9 // Corner roundness (0..1)
    , color: '#B2B2B2' // #rgb or #rrggbb or array of colors
    , opacity: 0.1 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1.5 // Rounds per second
    , trail: 60 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '52%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'absolute' // Element positioning
};

var _CaptionTransitions = [
    {$Duration: 900, $Clip: 3, $Easing: $JssorEasing$.$EaseInOutCubic},
    {$Duration: 900, $Clip: 12, $Easing: $JssorEasing$.$EaseInOutCubic},
    {
        $Duration: 600,
        $Zoom: 11,
        $Easing: {
            $Zoom: $JssorEasing$.$EaseInExpo,
            $Opacity: $JssorEasing$.$EaseLinear
        },
        $Opacity: 2
    },
    {
        $Duration: 600,
        x: -0.6,
        $Zoom: 11,
        $Easing: {
            $Left: $JssorEasing$.$EaseInCubic,
            $Zoom: $JssorEasing$.$EaseInCubic
        },
        $Opacity: 2
    },
    {
        $Duration: 600,
        y: -0.6,
        $Zoom: 11,
        $Easing: {
            $Top: $JssorEasing$.$EaseInCubic,
            $Zoom: $JssorEasing$.$EaseInCubic
        },
        $Opacity: 2
    },
    {
        $Duration: 700,
        y: -0.6,
        $Zoom: 1,
        $Easing: {
            $Top: $JssorEasing$.$EaseInCubic,
            $Zoom: $JssorEasing$.$EaseInCubic
        },
        $Opacity: 2
    },
    {
        $Duration: 700,
        $Zoom: 11,
        $Rotate: 1,
        $Easing: {
            $Zoom: $JssorEasing$.$EaseInExpo,
            $Opacity: $JssorEasing$.$EaseLinear,
            $Rotate: $JssorEasing$.$EaseInExpo
        },
        $Opacity: 2,
        $Round: {$Rotate: 0.8}
    },
    {
        $Duration: 700,
        x: -0.6,
        $Zoom: 11,
        $Rotate: 1,
        $Easing: {
            $Left: $JssorEasing$.$EaseInCubic,
            $Zoom: $JssorEasing$.$EaseInCubic,
            $Opacity: $JssorEasing$.$EaseLinear,
            $Rotate: $JssorEasing$.$EaseInCubic
        },
        $Opacity: 2,
        $Round: {$Rotate: 0.8}
    },
    {
        $Duration: 700,
        y: -0.6,
        $Zoom: 11,
        $Rotate: 1,
        $Easing: {
            $Top: $JssorEasing$.$EaseInCubic,
            $Zoom: $JssorEasing$.$EaseInCubic,
            $Opacity: $JssorEasing$.$EaseLinear,
            $Rotate: $JssorEasing$.$EaseInCubic
        },
        $Opacity: 2,
        $Round: {$Rotate: 0.8}
    },
    {
        $Duration: 700,
        x: -0.6,
        $Zoom: 1,
        $Rotate: 1,
        $Easing: {
            $Left: $JssorEasing$.$EaseInQuad,
            $Zoom: $JssorEasing$.$EaseInQuad,
            $Rotate: $JssorEasing$.$EaseInQuad,
            $Opacity: $JssorEasing$.$EaseOutQuad
        },
        $Opacity: 2,
        $Round: {$Rotate: 1.2}
    },
    {
        $Duration: 700,
        y: -0.6,
        $Zoom: 1,
        $Rotate: 1,
        $Easing: {
            $Top: $JssorEasing$.$EaseInQuad,
            $Zoom: $JssorEasing$.$EaseInQuad,
            $Rotate: $JssorEasing$.$EaseInQuad,
            $Opacity: $JssorEasing$.$EaseOutQuad
        },
        $Opacity: 2,
        $Round: {$Rotate: 1.2}
    },
    {
        $Duration: 900,
        x: -0.6,
        $Easing: {$Left: $JssorEasing$.$EaseInOutBack},
        $Opacity: 2
    },
    {
        $Duration: 900,
        y: -0.6,
        $Easing: {$Top: $JssorEasing$.$EaseInOutBack},
        $Opacity: 2
    }
];

var jssor_1_options = {
    $FillMode: 4,
    $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
    $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
    $Idle: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
    $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
    $Loop: 0,                                       //[Optional] Enable loop(circular) of carousel or not, 0: stop, 1: loop, 2 rewind, default value is 1
    $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
    $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
    $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
    //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
    //$SlideHeight: 500,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
    $SlideSpacing: 5, 					                //[Optional] Space between each slide in pixels, default value is 0
    $Cols: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
    $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
    $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
    $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
    $DragOrientation: 0,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)
    $ArrowNavigatorOptions: {
        $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
        $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
        $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
        $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
    },
    $CaptionSliderOptions: {                            //[Optional] Options which specifies how to animate caption
        $Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
        $CaptionTransitions: _CaptionTransitions,       //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
        $PlayInMode: 1,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
        $PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
    },
    $ThumbnailNavigatorOptions: {
        $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
        $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
        $ActionMode: 0,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
        $NoDrag: true                              //[Optional] Disable drag or not, default value is false
    }
};

function workMap() {
    window.workmap = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 50.4597651, lng: 30.5364448},
        zoom: 11
    });
    window.info = {};
}

function initMap() {
    window.map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 50.4597651, lng: 30.5364448},
        streetViewControl: true,
        zoom: 13
    });
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    window.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

    google.maps.event.addListenerOnce(map, 'idle', function () {
        var search = window.location.search;
        if (search.startsWith('?search')) {
            var search_param = search.split('=')[1];
            social_click(search_param);
        }
    });


    google.maps.event.addListener(searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return;
        }
        var bounds = new google.maps.LatLngBounds();
        window.map.setCenter(places[0].geometry.location);
        window.mapCircle.setMap(null);

        window.mapCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: window.map,
            editable: true,
            draggable: true,
            center: places[0].geometry.location,
            radius: 1000
        });


        // Bias the SearchBox results towards current map's viewport.
        window.map.addListener('bounds_changed', function () {
            searchBox.setBounds(window.map.getBounds());
        });
        google.maps.event.trigger(input, 'keydown', {
            keyCode: 13
        });
    });

    window.markers = {};
    window.contents = {};
    window.counter = 0;
    window.info = {};

    window.mapCircle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: window.map,
        editable: true,
        draggable: true,
        center: {lat: 50.4597651, lng: 30.5364448},
        radius: 1000
    });

    window.mapCircle.addListener('radius_changed', function () {
        if (window.mapCircle.getRadius() > 5000) {
            window.mapCircle.setRadius(5000)
        }
    });
}

function search_job(text) {
    var data = {
        'q': text
    };

    $.ajax({
        type: "POST",
        url: $(document).attr("href"),
        data: data,
        dataType: "json",
        success: function (resp) {
            addMarkers(resp.result, window.workmap, 'F65D41');
            $('#work_count').text(resp.work_count);
        }
    });
}

function send_coord() {
    text = $('#q');
    var fuzz = $("#fuzz");
    var target = document.getElementById('bar');
    var spinner = new Spinner(opts).spin(target);
    fuzz.css("height", $(document).height());
    fuzz.fadeIn();
    target.style.display = 'block';
    $('li').addClass('disabled');

    var center = window.mapCircle.center;
    var data = {
        'lat': center.lat(),
        'lng': center.lng(),
        'radius': window.mapCircle.radius,
        'q': text.val(),
        'search_param': $('#search_param').val(),
        'start_date': new Date($('#datestart').data().date).valueOf() / 1000,
        'end_date': new Date($('#dateend').data().date).valueOf() / 1000
    };

    var pinColorMap = {
        'Instagram': "517fa4",
        'Vkontakte': "45668e",
        'Twitter': "9DF641",
        'Foursquare': "41D9F6"
    };

    $.ajax({
        type: "POST",
        url: $(document).attr("href"),
        data: data,
        dataType: "json",
        success: function (resp) {
            window.contents = resp.result;
            window.span_count_ins += resp.count_ins;
            window.span_count_vk += resp.count_vk;
            window.span_count_four += resp.count_four;
            window.span_count_tw += resp.count_tw;
            var pinColor = "FE7569";
            addMarkers(resp.result, window.map, pinColorMap[data['search_param']]);
            $('#count_vk').text(window.span_count_vk);
            $('#count_instagram').text(window.span_count_ins);
            $('#count_tw').text(window.span_count_tw);
            $('#count_four').text(window.span_count_four);
            target.style.display = 'none';
            $("#fuzz").fadeOut();
            $('li').removeClass('disabled');
        }
    });
}

function addMarkers(content, maps, pincolor) {
    for (var key in content) {
        var sep = key.split(',');
        var position = {lat: parseFloat(sep[0]), lng: parseFloat(sep[1])};

        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pincolor,
            new google.maps.Size(21, 34),
            new google.maps.Point(0, 0),
            new google.maps.Point(10, 34));
        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
            new google.maps.Size(40, 37),
            new google.maps.Point(0, 0),
            new google.maps.Point(12, 35));
        var marker = new google.maps.Marker({
            position: position,
            map: maps,
            icon: pinImage,
            shadow: pinShadow
        });
        marker.set("id", key);
        var infowindow = new google.maps.InfoWindow({
            position: position,
            pixelOffset: new google.maps.Size(0, 100)
        });
        window.info[key] = infowindow;

        google.maps.event.addListener(infowindow, 'domready', function () {
            var ratings = $('.rating-loading');
            ratings.rating({
                min: 0,
                max: 10,
                step: 0.1,
                stars: 10,
                size: 'xs',
                showClear: false,
                showCaption: true,
                starCaptions: function (val) {
                    return "Rating " + val
                }
            });
            ratings.on('rating.change', function (event, value, caption) {
                    var company = this.parentNode.parentNode.childNodes[1].value;
                    send_rating(company, value);
            });

            w = parseInt(document.body.clientWidth / 3);
            h = parseInt(document.body.clientHeight / 2);
            e = $('#jssor_1');
            e.css('width', w);
            e.css('height', h);

            var jssor_slider1 = new $JssorSlider$("jssor_1", jssor_1_options);

            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    var sliderWidth = parentWidth;
                    sliderWidth = Math.min(sliderWidth, 810);
                    jssor_slider1.$ScaleWidth(sliderWidth);
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
        });

        google.maps.event.addListener(marker, 'click', function () {
            p = this.get("id");
            window.info[p].close();
            window.info[p].setContent(content[p]);
            window.info[p].open(maps, this);
        });
    }
}
