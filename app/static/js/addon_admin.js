/**
 * Created by n_kovganko on 25.01.2016.
 */

function update_document() {
    // for the Source template:

    $('input[name="location"]').after('<button type="button" style="position: relative;margin-left: 20px;" onclick="get_location();">Get location</button>');
    $('input[name="logo"]').after('<button type="button" style="position: relative;margin-left: 20px;" onclick="set_default();">Set default</button>');
}

function get_location() {
    var address = $('#id_addresses').val();
    $.ajax({
        type: "GET",
        url: 'http://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false',
        success: function (resp) {
            loc = resp.results[0].geometry.location.lat.toString() + ',' + resp.results[0].geometry.location.lng.toString();
            address = resp.results[0].formatted_address;
            $('#id_location').val(loc);
            $('#id_addresses').val(address);
        }
    });
}


function set_default() {
    $('#id_logo').val('/media/logo/22017cd6-12a6-4a65-82e1-a20e6563eaa4.png');
}


function logo_copy() {
    $.ajax({
        type: "GET",
        url: '/copy_logo',
        success: function (resp) {
        }
    });
}

// give time to jquery to load..
setTimeout("update_document();", 500);