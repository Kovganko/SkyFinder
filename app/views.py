# -*- coding: UTF-8 -*-
import ast
import os
from smtplib import SMTPRecipientsRefused
from django.contrib.auth import login, authenticate, logout
from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import ensure_csrf_cookie
from requests_futures.sessions import FuturesSession
from ipware.ip import get_real_ip, get_ip

from api.common import Checker, regenerate_content, add_ip_address_to_list, \
    get_localization
from api.google_api.core import BROWSER_API_KEY
from api.social.foursquare_api import handle_f, venue_explore, recent_checkins
from api.social.instagram_api import media_search, handle_i
from api.social.twitter_api import search_tweets, handle_tw
from api.social.vk_api import handle_vk_p, photos_search
from api.work.work_ua import WorkUa, DouUa, RabotaUa
from app.forms import UserForm, LoginForm, EditProfileForm
from app.models import Person, Notification, Company
from configuration.settings import MEDIA_ROOT


@ensure_csrf_cookie
def index(request):
    form = LoginForm()
    if request.user.is_authenticated():
        user_form = UserForm(instance=request.user)
    else:
        user_form = UserForm()
    d = {'API_KEY': BROWSER_API_KEY, 'social': True}
    result = {}
    if request.is_ajax():
        if 'q' in request.POST.keys():
            text = request.POST['q']
            r = int(float(request.POST['radius']))
            lat = request.POST['lat']
            lng = request.POST['lng']
            Checker.setup(request.POST['search_param'])
            session = FuturesSession()
            twitter_resp = search_tweets(session, lat, lng, r, text, 100,
                                         request.POST['end_date'])
            instagram_resp = media_search(session, lat, lng, r, 100,
                                          request.POST['start_date'],
                                          request.POST['end_date'])
            four_resp = venue_explore(session, lat, lng, r, text, 50)
            vk_resp = photos_search(session, text, lat, lng, r, 500)
            d['count_ins'] = handle_i(instagram_resp, result)
            d['count_four'] = handle_f(four_resp, result)
            d['count_tw'] = handle_tw(twitter_resp, result)
            d['count_vk'] = handle_vk_p(float(lat), float(lng), r, vk_resp,
                                        result)
            d['result'] = regenerate_content(result)
            return JsonResponse(d)
    current_lang = 'us'
    if 'lang' in request.COOKIES:
        current_lang = request.COOKIES['lang']
    if 'lang' in request.GET.keys():
        current_lang = request.GET['lang']
    d['current_lang'] = current_lang
    locale = get_localization(current_lang)
    d['locale'] = locale
    d['locale'].update({'current_lang': locale['%s_lang' % current_lang]})
    d['form'] = form
    d['user_form'] = user_form
    notification = Notification.objects.filter(active=True)
    d['notification'] = notification
    response = render(request, "index.html", d)
    response.set_cookie('lang', current_lang)
    return response


def home(request):
    return redirect('index')


def register(request):
    if request.method == 'POST':
        form = UserForm(request.POST, request.FILES)
        if form.is_valid():
            del form.cleaned_data['confirm_password']
            user = Person.objects.create(**form.cleaned_data)
            user.set_password(form.cleaned_data['password'])
            if user.photo.name is None:
                user.photo.name = '/static/img/default_profile.png'
            ip1 = get_real_ip(request, right_most_proxy=True)
            if ip1 is not None:
                user.last_ip = ip1
                add_ip_address_to_list(user, ip1)
            else:
                user.last_ip = get_ip(request, right_most_proxy=True)
                add_ip_address_to_list(user, user.last_ip)
            user.save()
            user = authenticate(username=form.cleaned_data['username'],
                                password=form.cleaned_data['password'])
            login(request, user)
            return JsonResponse({})
        else:
            password1 = form.cleaned_data.get('password')
            password2 = form.cleaned_data.get('confirm_password')
            username = form.cleaned_data.get('username')
            email = form.cleaned_data.get('email')
            if password1 is None or password2 is None:
                return JsonResponse({'error': 'Passwords is required.'})
            if password1 and password1 != password2:
                return JsonResponse({'error': 'Passwords don\'t match.'})
            if len(password1) < 6:
                return JsonResponse(
                    {'error': 'Passwords is too small. '
                              'Minimum length password 6. '})
            if Person.objects.filter(username=username).exists():
                return JsonResponse(
                    {'error': "%s already exist, "
                              "please enter another value." % username})
            if Person.objects.filter(email=email).exists():
                return JsonResponse(
                    {'error': "%s already exist, "
                              "please enter another email." % email})


def signin(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['username'],
                            password=request.POST['password'])
        if user is not None:
            ip1 = get_real_ip(request, right_most_proxy=True)
            if ip1 is not None:
                user.last_ip = ip1
                add_ip_address_to_list(user, ip1)
            else:
                user.last_ip = get_ip(request, right_most_proxy=True)
                add_ip_address_to_list(user, user.last_ip)
            user.save()
            login(request, user)
            return JsonResponse({})
    return JsonResponse({'error': 'Wrong username or password'})


def logout_user(request):
    logout(request)
    return redirect('index')


def sitemap(request):
    return render(request, 'sitemap.xml')


def robots(request):
    return render(request, 'robots.txt')


def works(request):
    d = {'API_KEY': BROWSER_API_KEY, 'work': True}
    result = {}
    if 'q' in request.POST.keys():
        text = request.POST['q']
        worker1 = WorkUa('//*/div[contains(@class, "job-link")]')
        worker1.request(text)
        worker2 = DouUa('.//*[@class="vacancy"]/*[@class="title"]')
        worker2.request(text)
        worker3 = RabotaUa('//tr[contains(@class, "v")]/td/div')
        worker3.request(text)
        d['work_count'] = worker2.parse_response(result)
        d['work_count'] += worker1.parse_response(result)
        d['work_count'] += worker3.parse_response(result)
        regenerate_content(result)
        d['result'] = result
        return JsonResponse(d)
    form = LoginForm()
    user_form = UserForm()
    d['form'] = form
    d['user_form'] = user_form
    notification = Notification.objects.filter(active=True)
    d['notification'] = notification
    current_lang = 'us'
    if 'lang' in request.COOKIES:
        current_lang = request.COOKIES['lang']
    if 'lang' in request.GET.keys():
        current_lang = request.GET['lang']
    d['current_lang'] = current_lang
    locale = get_localization(current_lang)
    d['locale'] = locale
    d['locale'].update({'current_lang': locale['%s_lang' % current_lang]})
    return render(request, 'works.html', d)


def test(request):
    return render(request, 'test.html')


def remind_username(request):
    current_lang = 'us'
    if 'lang' in request.COOKIES:
        current_lang = request.COOKIES['lang']
    locale = get_localization(current_lang)
    if request.is_ajax():
        email = request.POST['email']
        try:
            user = Person.objects.filter(email=email)
            if user.exists():
                f_name = user[0].first_name
                who = email if not f_name else f_name
                message = locale['remind_username_message'] % (
                    who, user[0].username)
                send_mail(locale['restore_username'], message,
                          'support@skyfinder.me', [email], fail_silently=False)
            else:
                return JsonResponse(
                    {'error': locale['restore_username_not_found'] % email})
        except SMTPRecipientsRefused:
            return JsonResponse(
                    {'error': locale['restore_incorrect_email'] % email})
        except Exception as e:
            return JsonResponse({'error': str(e.__class__.__name__) + str(e)})
    return JsonResponse({})


def remind_password(request):
    current_lang = 'us'
    if 'lang' in request.COOKIES:
        current_lang = request.COOKIES['lang']
    locale = get_localization(current_lang)
    if request.is_ajax():
        username = request.POST['username']
        try:
            user = Person.objects.filter(username=username)
            if user.exists():
                f_name = user[0].first_name
                email = user[0].email
                password = user[0].password
                url = 'http://skyfinder.me/restore_password' \
                      '_form?username=%s&hash=%s' % (username, password)
                who = username if not f_name else f_name
                message = locale['remind_password_message'] % (who, url)
                send_mail(locale['restore_password'], message,
                          'support@skyfinder.me', [email], fail_silently=False)
            else:
                return JsonResponse(
                    {'error': locale['restore_password_not_found'] % username})
        except SMTPRecipientsRefused:
            return JsonResponse(
                    {'error': locale['restore_incorrect_email'] % email})
        except Exception as e:
            return JsonResponse({'error': str(e.__class__.__name__) + str(e)})
    return JsonResponse({})


def send_message(request):
    current_lang = 'us'
    if 'lang' in request.COOKIES:
        current_lang = request.COOKIES['lang']
    locale = get_localization(current_lang)
    if request.is_ajax():
        email = request.POST['inputEmail']
        name = request.POST['inputName']
        subject = request.POST['inputSubject']
        message = 'From %s:\r\n%s' % (name, request.POST['inputMessage'])
        try:
            send_mail(subject, message, email, ['support@skyfinder.me'],
                      fail_silently=False)
        except SMTPRecipientsRefused:
            return JsonResponse(
                    {'error': locale['restore_incorrect_email'] % email})
        except Exception as e:
            return JsonResponse({'error': str(e.__class__.__name__) + str(e)})
    return JsonResponse({})


def restore_password_form(request):
    username = None
    if 'username' in request.GET and 'hash' in request.GET:
        username = request.GET['username']
        hash_value = request.GET['hash'].replace(' ', '+')
        user = Person.objects.filter(username=username, password=hash_value)
        if user.exists():
            return render(request, 'restore_password_form.html',
                          {'username': username})
    return JsonResponse({'validation-error': 'AuthValidationError',
                         'username': username})


def reset_password(request):
    password = request.POST['password']
    username = request.POST['username']
    user = Person.objects.get(username=username)
    user.set_password(password)
    user.save()
    return redirect('index')


def handle_uploaded_file(f):
    with open(os.path.join(MEDIA_ROOT, 'users', str(f)), 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def change_profile(request):
    if request.method == 'POST':
        handle_uploaded_file(request.FILES['photo_upload'])
        user = Person.objects.get(username=request.user.username)
        user.photo.name = 'users/' + str(request.FILES['photo_upload'])
        user.save()
    return JsonResponse({'success': 'true'})


def update_settings(request):
    if request.method == 'POST':
        try:
            email = request.POST['email']
            user = Person.objects.get(username=request.user.username)
            user.email = email
            user.phone = request.POST['phone']
            user.city = request.POST['city']
            user.country = request.POST['country']
            user.save()
        except Exception as e:
            return JsonResponse({'error': str(e)})
        return JsonResponse({})


def send_rating(request):
    current_lang = 'us'
    if 'lang' in request.COOKIES:
        current_lang = request.COOKIES['lang']
    locale = get_localization(current_lang)
    if request.method == 'POST':
        company_name = request.POST['company']
        rating = float(request.POST['rating'])
        user = Person.objects.get(username=request.user.username)
        if not user.is_authenticated():
            JsonResponse({'error': locale['voted_error1']})
        if not user.is_voted_company(company_name):
            company = Company.objects.get(name=company_name)
            if company is not None:
                old = company.rating
                count = company.count_voted
                company.rating = ((count*old) + rating)/(count + 1)
                company.count_voted += 1
                voted_company = ast.literal_eval(user.voted_company)
                voted_company.append(company_name)
                user.voted_company = str(voted_company)
                user.save()
                company.save()
        else:
            return JsonResponse({'error': locale['voted_error2']})
    return JsonResponse({'success': 'true'})


def find_checkins_friends(request):
    current_lang = 'us'
    if 'lang' in request.COOKIES:
        current_lang = request.COOKIES['lang']
    locale = get_localization(current_lang)
    d = {'API_KEY': BROWSER_API_KEY, 'social': True}
    if request.method == 'POST':
        if request.user.is_anonymous():
            return JsonResponse(
                {'error': locale['error_registration_checkin']})
        user = Person.objects.get(username=request.user.username)
        if user.fourquare_id is None:
            return JsonResponse(
                {'error': locale['error_registration_checkin2']})
        result = recent_checkins(user.fourquare_token)
        if len(result) == 0:
            return JsonResponse(
                {'error': locale['error_registration_checkin3']})
        regenerate_content(result)
        d['result'] = result
        return JsonResponse(d)


def send_foursquare_info(request):
    if request.method == 'POST':
        user = Person.objects.get(username=request.user.username)
        user.fourquare_id = request.POST['foursquare_id']
        user.fourquare_token = request.POST['token']
        user.save()
        return JsonResponse({})
