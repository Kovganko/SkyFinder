# -*- coding: UTF-8 -*-

from django import forms
from django.forms import ModelForm
from django_countries.widgets import CountrySelectWidget

from app.models import Person


class UserForm(ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'input-sm form-control'}))
    photo = forms.FileField(required=False, widget=forms.FileInput(attrs={'class': 'file', 'data-show-upload': 'false',
                                                                          'data-allowed-file-extensions': '["png", "jpg", "jpeg"]'}))

    class Meta:
        model = Person
        fields = ('username', 'email', 'password', 'country', 'city', 'phone')
        l = '<div class="col-sm-2"><img id="{flag_id}" src="{country.flag}"></div>' \
            '<div class="col-sm-10" style="padding-right:0;padding-left:0;">{widget}</div>'
        widgets = {'country': CountrySelectWidget(attrs={'class': 'input-sm form-control'}, layout=l),
                   'password': forms.PasswordInput(attrs={'class': 'input-sm form-control'}),
                   'username': forms.TextInput(attrs={'class': 'input-sm form-control'}),
                   'email': forms.TextInput(attrs={'class': 'input-sm form-control'}),
                   'city': forms.TextInput(attrs={'class': 'input-sm form-control'}),
                   'phone': forms.TextInput(attrs={'class': 'input-sm form-control'})}

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserForm, self).save(commit=commit)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class LoginForm(ModelForm):
    class Meta:
        model = Person
        fields = ('username', 'password')
        widgets = {'password': forms.PasswordInput(attrs={'class': 'input-sm form-control'}),
                   'username': forms.TextInput(attrs={'class': 'input-sm form-control'})}


class EditProfileForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.fields["vk_sid"].initial = self.instance.vk_sid
        self.fields["vk_first_name"].initial = self.instance.vk_first_name
        self.fields["vk_last_name"].initial = self.instance.vk_last_name
        self.fields["vk_href"].initial = self.instance.vk_href
        self.fields["vk_id"].initial = self.instance.vk_id
        self.fields["vk_expire"].initial = self.instance.vk_expire

    vk_sid = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}))
    vk_first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}))
    vk_last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}))
    vk_href = forms.URLField(widget=forms.URLInput(attrs={'class': 'form-control', 'readonly': 'readonly'}))
    vk_id = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}))
    vk_expire = forms.CharField(widget=forms.DateTimeInput(attrs={'class': 'form-control', 'readonly': 'readonly'}, ))

    class Meta:
        model = Person
        fields = ('username', 'email', 'password', 'country', 'city', 'phone', 'photo')
        l = '<span class="input-group-addon"><img id="{flag_id}" src="{country.flag}"></span>{widget}'
        widgets = {'country': CountrySelectWidget(layout=l),
                   'username': forms.TextInput(attrs={'readonly': 'readonly', 'class': 'form-control'}),
                   'email': forms.EmailInput(attrs={'class': 'form-control'}),
                   'password': forms.PasswordInput(attrs={'class': 'form-control'}, render_value=True),
                   'photo': forms.FileInput(attrs={'class': 'file file-loading', 'data-show-upload': 'false',
                                                   'data-allowed-file-extensions': '["png", "jpg", "jpeg"]'})}

    def save(self, commit=True):
        user = super(EditProfileForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        user.vk_sid = self.cleaned_data["vk_sid"]
        user.vk_first_name = self.cleaned_data["vk_first_name"]
        user.vk_last_name = self.cleaned_data["vk_last_name"]
        user.vk_href = self.cleaned_data["vk_href"]
        user.vk_id = self.cleaned_data["vk_id"]
        user.vk_expire = self.cleaned_data["vk_expire"]
        user.save()
        return user
