# -*- coding: UTF-8 -*-

__author__ = 'n_kovganko'

import ast

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.mail import send_mail
from django.db import models
from django_countries.fields import CountryField


class ListField(models.TextField):
    __metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value

        return str(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)


class UserProfileManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
                username=username,
                email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(email=email,
                                password=password,
                                username=username,
                                )
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class Person(AbstractBaseUser):
    username = models.CharField(max_length=20, null=False, blank=False, unique=True)
    email = models.EmailField(max_length=40, null=False, blank=False)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    country = CountryField(null=True, blank=True)
    city = models.CharField(max_length=20, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    created = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default=False)
    photo = models.FileField(default='/static/img/default_profile.png', upload_to="users", null=True, blank=True)
    last_ip = models.GenericIPAddressField(null=True, blank=True)
    ip_addresses = models.TextField(max_length=85, null=True, blank=True)
    voted_company = ListField(default=[])
    fourquare_id = models.CharField(max_length=20, null=True, blank=True)
    fourquare_token = models.CharField(max_length=60, null=True, blank=True)

    def has_photo(self):
        if self.photo.name == '/static/img/default_profile.png':
            return False
        return True
    has_photo.boolean = True

    def is_voted_company(self, company):
        if company in ast.literal_eval(self.voted_company):
            return True
        return False

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = UserProfileManager()

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def thumbnail(self):
        return """<a href="/media/users/%s"><img border="0" alt="" src="/media/users/%s" height="40" /></a>""" % (
            (self.photo.name, self.photo.name))

    thumbnail.allow_tags = True


class Notification(models.Model):
    active = models.BooleanField(default=False)
    message = models.TextField()


class Company(models.Model):
    name = models.CharField(db_index=True, max_length=70, null=False, blank=False, unique=True)
    addresses = models.TextField(db_index=True, max_length=150, null=True, blank=True)
    location = models.CharField(db_index=True, max_length=40, null=True, blank=True)
    logo = models.CharField(db_index=True, max_length=150, null=True, blank=True)
    link = models.URLField(db_index=True, null=True, blank=True)
    description = models.TextField(db_index=True, max_length=600, null=True, blank=True)
    rating = models.FloatField(default=0.0, null=False, blank=False)
    count_voted = models.IntegerField(default=0, null=False, blank=False)

    def handling(self):
        if self.logo is None:
            return False
        if self.logo.startswith('/media/logo/') and self.description is not None and self.link is not None:
            return True
        return False
    handling.boolean = True

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    @classmethod
    def wait_handling(cls):
        data = set()
        for item in Company.objects.all():
            if not item.handling():
                data.add(item.pk)
        return Company.objects.filter(pk__in=data)

    @classmethod
    def without_address(cls):
        data = set()
        for item in Company.objects.all():
            if item.addresses is None or item.addresses == 'Kiev, Ukraine, 02000':
                data.add(item.pk)
        return Company.objects.filter(pk__in=data)
