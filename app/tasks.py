# -*- coding: UTF-8 -*-

import requests
import shutil
from celery import Celery
from celery.task import task
import os

from lxml import html
from lxml.etree import _ElementUnicodeResult
from api.common import generate_string, compare_strings
from api.google_api.core import web_search, search_place
from app.models import Company
from configuration.settings import BROKER_URL, CELERY_RESULT_BACKEND

celery = Celery('tasks', broker=BROKER_URL, backend=CELERY_RESULT_BACKEND)

os.environ['DJANGO_SETTINGS_MODULE'] = "configuration.settings"


@task(name='load_logo')
def load_logo():
    companies = Company.objects.all()
    for i in companies:
        if i.logo is not None:
            if not i.logo.startswith('/media/logo'):
                try:
                    r = requests.get(i.logo.replace('https', 'http'), stream=True, verify=False)
                    if r.status_code == 200:
                        if i.logo is not None:
                            suffix = i.logo.split('.')[-1]
                            path = generate_string() + '.' + suffix
                            logo = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'media', 'logo', path)
                            if os.path.exists(logo):
                                path = generate_string() + '.' + suffix
                                logo = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'media', 'logo', path)
                            with open(logo, 'wb') as f:
                                r.raw.decode_content = True
                                shutil.copyfileobj(r.raw, f)
                                i.logo = '/media/logo/%s' % path
                                i.save(force_update=True)
                except Exception:
                    pass


@task(name='find_info')
def find_info():
    company = Company.wait_handling()
    for i in company:
        search_info(i)


def search_info(model):
    result = web_search(model.name + '+работа+о+компании')
    for i in result:
        try:
            url = i['url']
            if url.startswith('http://www.work.ua/jobs/by-company/'):
                resp = requests.get(url)
                doc = html.fromstring(resp.text)
                try:
                    c_link = doc.xpath('//div[@class="website-company"]/a/@href')[0]
                    if model.link is None:
                        model.link = c_link
                        model.save()
                except IndexError:
                    pass
                try:
                    info_text = ''
                    info = doc.xpath('//div[@class="card wordwrap"]/p/node()')
                    for j in info:
                        if isinstance(j, _ElementUnicodeResult):
                            info_text += str(j)
                        else:
                            if j.text is not None:
                                info_text += j.text
                    if len(info_text) > 599:
                        info_text = info_text[:600]
                    if model.description is None:
                        model.description = info_text
                        model.save()
                except IndexError:
                    pass

            if url.startswith('http://jobs.dou.ua/companies/'):
                resp = requests.get(url)
                doc = html.fromstring(resp.text)
                try:
                    logo = doc.xpath('//img[@class="logo"]/@src')[0]
                    if model.logo is None:
                        model.logo = logo
                        model.save()
                except IndexError:
                    pass
                try:
                    site = doc.xpath('//div[@class="site"]/a/@href')[0]
                    if model.link is None:
                        model.link = site
                        model.save()
                except IndexError:
                    pass
                try:
                    info = doc.xpath('//div[@class="b-typo"]/div/node()')
                    info_text = ''
                    for j in info:
                        if isinstance(j, _ElementUnicodeResult):
                            info_text += str(j)
                        else:
                            if j.text is not None:
                                info_text += j.text
                    if len(info_text) > 599:
                        info_text = info_text[:600]
                    if model.description is None:
                        model.description = info_text
                        model.save()
                except IndexError:
                    pass

            if url.startswith('http://rabota.ua/company'):
                resp = requests.get(url)
                doc = html.fromstring(resp.text)
                try:
                    c_link = doc.xpath('//a[contains(@class, "cmp_site")]/@href')[0]
                    if model.link is None:
                        model.link = c_link
                        model.save()
                except IndexError:
                    pass
                try:
                    info = doc.xpath('//div[contains(@class, "company-desc")]/text()')[0]
                    if len(info) > 599:
                        info = info[:600]
                    if model.description is None:
                        model.description = info
                        model.save()
                except IndexError:
                    pass
        except requests.ConnectionError:
            pass
    place = search_place(model.name + '+kiev')
    if place:
        if compare_strings(place.name, model.name) >= 50:
            model.addresses = place.formatted_address
            model.location = place.location_str
            model.save()
