__author__ = 'n_kovganko'
import json
import math

from datetime import datetime

from api.common import Checker, render_template



CLIENT_ID = 5180102
SECRET_KEY = 'l5IQG2Psy2XbxGweYjBJ'
ACCESS_TOKEN = '4b062bba498a823226b6d7095e0f90c6c83ae7c495f20f09b3f63aed34533a56b3b5fc654ab9ab8f5d49a'


def rad2deg(radians):
    pi = math.pi
    degrees = 180 * radians / pi
    return degrees


def in_circle(center_x, center_y, radius, x, y):
    square_dist = (center_x - x) ** 2 + (center_y - y) ** 2
    return square_dist <= rad2deg(radius / 1000 / 6367) ** 2


def photos_search(session, q, lat, lng, distance, count):
    if int(count) == 0:
        return None
    if Checker.vk_check:
        if q == '':
            q = ' '
        params = 'q={5}&access_token={0}&lat={1}&long={2}&radius={3}&count={4}'.format(ACCESS_TOKEN, lat, lng, distance, count, q)
        return session.get('https://api.vk.com/method/photos.search?' + params, verify=False)


def handle_vk_p(lat, lng, distance, content, _array):
    lentgh = 0
    if Checker.vk_check:
        if content is None:
            return {}
        mediaList = json.loads(content.result().text)
        for media in mediaList['response']:
            if isinstance(media, dict):
                if 'lat' in media:
                    if in_circle(lat, lng, distance, media['lat'], media['long']):
                        created_time = datetime.fromtimestamp(media['created']).strftime("%a %b %y %H:%M:%S %Y")
                        location = '%s,%s' % (media['lat'], media['long'])
                        image = media['src_big']
                        caption = media['text']
                        if caption != '':
                            caption = '<div u=caption t="*" class="captionOrange" style="position:absolute; ' \
                                      'left:20px; top: 30px; width:300px; height:30px;">{0}</div>'.format(caption)
                        owner_id = media['owner_id']
                        username = 'https://vk.com/id' + str(owner_id)
                        content = render_template('vk.html', username, image, caption, created_time)
                        if location in _array.keys():
                            _array[location].append(content)
                        else:
                            _array[location] = [content]
                        lentgh += 1
    return lentgh
