import json
from datetime import datetime
import time
from api.common import Checker, render_template

instagram_client_id = '185a92f05e8c41d38f277e2d5332f720'
instagram_secret = 'abf5fc81b89946d8b10bbc53f8c0ede1'
instagram_token = '2309026616.1677ed0.defd2206e5ee4c9aa025bc8b720224c5'


def media_search(session, lat, lng, distance, count, start_date, end_date):
    if int(count) == 0:
        return None
    if Checker.instagram_check:
        params = 'access_token={0}&lat={1}&lng={2}&distance={3}&count={4}'.format(instagram_token, lat, lng,
                                                                                  distance, count)
        if start_date != 'NaN':
            params += '&min_timestamp=%s' % str(start_date)
        if end_date != 'NaN':
            params += '&max_timestamp=%s' % str(end_date)
        return session.get('http://api.instagram.com/v1/media/search?' + params)


def handle_i(content, _array):
    lentgh = 0
    if Checker.instagram_check:
        if content is None:
            return {}
        result = content.result()
        mediaList = json.loads(result.text)
        for media in mediaList['data']:
            caption = ''
            link = media['link']
            image = media['images']['standard_resolution']['url']
            if media['caption'] is not None:
                caption = media['caption']['text']
            if caption != '':
                caption = '<div u=caption t="*" class="captionOrange" style="position:absolute; ' \
                          'left:20px; top: 30px; width:300px;">{0}</div>'.format(caption)
            created = datetime.fromtimestamp(int(media['created_time'])).strftime("%a %b %y %H:%M:%S %Y")
            location = '%s,%s' % (media['location']['latitude'], media['location']['longitude'])
            content = render_template('instagram.html', link, image, caption, created)
            if location in _array.keys():
                _array[location].append(content)
            else:
                _array[location] = [content]
            lentgh += 1
    return lentgh
