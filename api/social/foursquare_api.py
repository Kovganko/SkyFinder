from datetime import datetime
import json

import requests

from api.common import Checker, render_template

CLIENT_ID = 'F4UR2WCEL4C2WLHDKIFW0GYF5DCFUAG5HNZMHVSB4GFH4MLF'
CLIENT_SECRET = 'O5QR1A0YZJDAJFY54FFIAP1BMPRLAFEAPFFIDZOYVQU1SAHO'
TOKEN = 'T5YRE1GSNA2NTNJMYLEXUI0F0AO1GEPWVINLSPW0U0U2LWJG'


def resolve(session, shord_id):
    date = datetime.now().strftime('%Y%m%d')
    params = 'shortId={0}&oauth_token={1}&v={2}'.format(shord_id, TOKEN, date)
    return session.get(
        'https://api.foursquare.com/v2/checkins/resolve?' + params)


def venue_search(session, lat, lng, distance, q, count=50):
    if int(count) == 0:
        return None
    date = datetime.now().strftime('%Y%m%d')
    params = 'll={0},{1}&oauth_token={2}&v={3}&limit={4}' \
             '&radius={5}'.format(lat, lng, TOKEN, date, count, distance)
    if q != '':
        params += '&query={0}'.format(q)
    return session.get('https://api.foursquare.com/v2/venues/search?' + params)


def my_friends(user_id):
    date = datetime.now().strftime('%Y%m%d')
    params = 'oauth_token={2}&v={3}&limit={4}'.format(TOKEN, date, 500)
    resp = requests.get(
        'https://api.foursquare.com/v2/users/%s/friends?' % user_id + params)
    response = json.loads(resp.text)
    result = []
    if response['meta']['code'] == 200:
        for user in response['response']['friends']['groups'][1]['items']:
            result.append(user['id'])
    return result


def recent_checkins(token):
    date = datetime.now().strftime('%Y%m%d')
    params = 'oauth_token={0}&v={1}'.format(token, date)
    resp = requests.get(
        'https://api.foursquare.com/v2/checkins/recent?' + params)
    response = json.loads(resp.text)
    result = {}
    if response['meta']['code'] == 200:
        for checkin in response['response']['recent']:
            user_id = checkin['user']['id']
            firstName = checkin['user']['firstName']
            lastName = checkin['user']['lastName']
            photo = checkin['user']['photo']['prefix'] + '128x128' + checkin['user']['photo']['suffix']
            checkin_id = checkin['id']
            venue_name = checkin['venue']['name']
            formattedPhone = checkin['venue']['contact']['formattedPhone']
            created = datetime.fromtimestamp(checkin['createdAt']).strftime("%B %d %Y")
            try:
                shout = checkin['shout']
            except KeyError:
                shout = ''
            location = str(checkin['venue']['location']['lat']) + ',' + str(checkin['venue']['location']['lng'])
            address = checkin['venue']['location']['address']
            content = render_template('foursquare.html', photo, firstName, lastName, shout, venue_name, address, formattedPhone, created)
            if location in result.keys():
                result[location].append(content)
            else:
                result[location] = [content]
    return result


def venue_explore(session, lat, lng, distance, q, count=50):
    if int(count) == 0:
        return None
    if Checker.foursquare_check:
        date = datetime.now().strftime('%Y%m%d')
        params = 'll={0},{1}&oauth_token={2}&v={3}&limit={4}&radius={5}' \
                 '&venuePhotos=1&m=foursquare'.format(
            lat, lng, TOKEN, date, count, distance)
        if q != '':
            params += '&query={0}'.format(q)
        return session.get(
            'https://api.foursquare.com/v2/venues/explore?' + params)


def photos(session, vid):
    params = 'oauth_token={0}&VENUE_ID={1}'.format(TOKEN, vid)
    return session.get(
        'https://api.foursquare.com/v2/venues/VENUE_ID/photos?' + params)


def handle_f(content, _array):
    lentgh = 0
    if Checker.foursquare_check:
        if content is None:
            return {}
        mediaList = json.loads(content.result().text)
        for media in mediaList['response']['groups'][0]['items']:
            venue = media['venue']
            likes_summary = tips_url = tips_data = rating = ''
            if 'tips' in media:
                tips = media['tips'][0]
                tips_created = datetime.fromtimestamp(tips['createdAt']).strftime("%B %d %Y")
                if 'likes' in tips:
                    likes_summary = '<div class="row" style="position: absolute;width:200px;left:200px; top: 160px;' \
                                    'font-size: 14px; color: #182534">Likes: {0}</div>'.format(tips['likes']['summary'])
                tips_text = tips['text']
                if 'canonicalUrl' in tips:
                    tips_url = '<a href="{0}"><{1}</a>'.format(tips['canonicalUrl'], tips_text)
                tips_user_id = tips['user']['id']
                last_name = ''
                if 'lastName' in tips['user']:
                    last_name = tips['user']['lastName']
                tips_user = '%s %s' % (tips['user']['firstName'], last_name)
                tips_user_photo = tips['user']['photo']['prefix'] + '32x32' + tips['user']['photo']['suffix']
                tips_data = '<div class="row" style="position:absolute;left:50px;width:500px;top:200px;color: #aeb4b6">' \
                            '<a href="https://ru.foursquare.com/user/{4}" target="_blank">' \
                            '<img style="position:relative;margin-right:10px;" src="{0}" width="32px" height="32px;">' \
                            '{1}&rarr;{2}</a></div>' \
                            '<div class="row" style="position:absolute;left:50px;top:250px;width:400px;">{3}' \
                            '</div>'.format(tips_user_photo, tips_user, tips_created, tips_url, tips_user_id)
            location = venue['location']
            location_str = '%s,%s' % (location['lat'], location['lng'])
            name = venue['name']
            venue_id = venue['id']
            categoty = venue['categories'][0]['name']
            if 'rating' in venue:
                rating = '<div class="row" style="position: absolute;left:600px;top:25px;width:30px;height:30px;' \
                         'padding:5px 5px;font-size: 12px; background-color: #00b551; color: #fff; ' \
                         'border-radius: 3px;">{0}</div>'.format(venue['rating'])
            try:
                p_url = venue['photos']['groups'][0]['items'][0]['prefix'] + '300x300' + \
                        venue['photos']['groups'][0]['items'][0]['suffix']
                venue_photo = '<a href="https://ru.foursquare.com/v/{1}/{2}" target="_blank"><img src="{0}"' \
                              ' width="150px" height="150px" style="position:absolute;left:20px;top:30px;"/>' \
                              '</a>'.format(p_url, categoty, venue_id)
            except IndexError:
                venue_photo = 'img/foursquare-default.png'
            if 'url' in venue:
                name = '<a href="%s">%s</a>' % (venue['url'], name)
            if 'address' in location:
                location_address = location['address']
            else:
                if 'city' in location:
                    location_address = location['city']
                else:
                    if 'country' in location:
                        location_address = location['country']
                    else:
                        location_address = location['state']
            checkin_count = '<div class="row" style="position: absolute;width:200px;left:200px; top: 120px;' \
                            'font-size: 14px; color: #182534">Checkins count: {0}</div>'.format(
                    venue['stats']['checkinsCount'])

            here_now = '<div class="row" style="position: absolute;width:200px;left:200px; top: 140px;' \
                       'font-size: 14px; color: #182534">Here now: {0}</div>'.format(venue['hereNow']['count'])
            content = """
                <div style="background: #ffffff;">
                    <div>
                        {0}
                        <div class="row" style="position: absolute;width:380px;left:200px; top: 30px;font-size: 20px; color: #2d5be3" ><strong>{1}</strong></div>
                         <div class="row" style="position: absolute;width:250px;left:200px; top: 60px;font-size: 14px; color: #959b9e" >{2}</div>
                        <div class="row" style="position: absolute;width:300px;left:200px; top: 80px;font-size: 14px; color: #959b9e" >{3}</div>
                        {4}{5}{6}{7}{8}
                    </div>
                    <div u="thumb" style="position: absolute;width:0;">
                    <a style="color:#EB5100" href="https://ru.foursquare.com/">https://ru.foursquare.com/</a></div>
                </div>""".format(venue_photo, name, categoty, location_address, rating, tips_data, checkin_count, here_now,
                                 likes_summary)
            lentgh += 1
            if location_str in _array.keys():
                _array[location_str].append(content)
            else:
                _array[location_str] = [content]
    return lentgh
