import json
import re
from datetime import datetime
from tweepy.auth import AppAuthHandler

from api.common import render_template, regenerate_content, Checker

CONSUMER_KEY = 'n8FNkVnLPhQFS1J6bSlNMCs5F'
CONSUMER_SECRET = '2X5cmPDT7ofMFpUlCfjcfWpahxygo0FE3KsC0F4VPHDuDYx0N7'
ACCESS_TOKEN = '4296168803-CX6adIIAIQEcFXqzTOAblJaIJfTIWdGO4iI8zjG'
ACCESS_TOKEN_SECRET = 'Zw9Se070RSkeQHBmF14J7It74GqlQ2wxpYForP5hsTQwc'
auth = AppAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
bearer_token = auth._bearer_token


def search_tweets(session, lat, lng, distance, q, count, end_date):
    if int(count) == 0:
        return None
    if Checker.twitter_check:
        headers = {
            'Authorization': 'Bearer {}'.format(bearer_token),
            'Accept-Encoding': 'gzip',
        }

        result_type = 'mixed'
        if q == '':
            q = '@'
        geocode = '%s,%s,%skm' % (lat, lng, distance / 1000)
        params = 'q={0}&result_type={1}&count={2}&geocode={3}'.format(q, result_type, count, geocode)
        if end_date != 'NaN':
            until = datetime.fromtimestamp(int(end_date)).strftime("%y/%m/%d")
            params += '&max_timestamp=%s' % str(until)
        return session.get('https://api.twitter.com/1.1/search/tweets.json?' + params, headers=headers)


def show_status(session, _id):
    headers = {
        'Authorization': 'Bearer {}'.format(bearer_token),
        'Accept-Encoding': 'gzip',
    }
    return session.get('https://api.twitter.com/1.1/statuses/show.json?id=%s' % _id, headers=headers)


def handle_tw(content, _array):
    lentgh = 0
    if Checker.twitter_check:
        if content is None:
            return {}
        mediaList = json.loads(content.result().text)
        for media in mediaList['statuses']:
            if 'geo' in media:
                caption = ''
                e_image = None
                url_m = None
                try:
                    expanded_url = media['entities']['urls'][0]['expanded_url']
                except IndexError:
                    expanded_url = ''
                try:
                    g_url = media['entities']['urls'][0]['url']
                except IndexError:
                    g_url = ''
                if 'media' in media['entities']:
                    try:
                        e_image = media['entities']['media'][0]['media_url']
                        url_m = media['entities']['media'][0]['url']
                    except IndexError:
                        pass
                url = media['user']['url']
                screen_name = media['user']['screen_name']
                username = media['user']['name']
                link = 'https://twitter.com/%s/status/%s' % (screen_name, media['id'])
                image = media['user']['profile_image_url']
                profile_text_color = media['user']['profile_text_color']
                if media['text'] is not None:
                    caption = media['text']
                    for match in re.finditer('(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?', caption, re.S):
                        temp_url = match.group(0)
                        if temp_url == g_url:
                            caption = caption.replace(temp_url, '<a href="{0}">{0}</a>'.format(expanded_url))
                        if image is not None and temp_url == url_m:
                            caption = caption.replace(temp_url, '<div class="row"><img src="{0}"/></div>'.format(e_image))
                created = datetime.strptime(media['created_at'], "%a %b %y %H:%M:%S %z %Y").strftime("%a %b %y %H:%M:%S %Y")
                location = '%s,%s' % (media['geo']['coordinates'][0], media['geo']['coordinates'][1])
                if url != '':
                    url = '<a href="{0}">{0}</a>'.format(url)
                content = render_template('twitter.html', image, link, caption, created, username,
                                          screen_name, profile_text_color, url)
                lentgh += 1
            if location in _array.keys():
                _array[location].append(content)
            else:
                _array[location] = [content]
    return lentgh
