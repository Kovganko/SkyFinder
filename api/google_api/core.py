import json
import requests

__author__ = 'n_kovganko'

SERVER_API_KEY = 'AIzaSyAIgK6YP8F9Tnz9ZKJqs5ohrqAwCDkzXEw'
BROWSER_API_KEY = 'AIzaSyAj_mD8yg4BjwoZPGlXRfOcJqDy6Csl1Zg'


class Place:
    def __init__(self, lat, lng, address, name):
        self.lat = lat
        self.lng = lng
        self.formatted_address = address
        self.location_str = '%s,%s' % (self.lat, self.lng)
        self.name = name


def web_search(query):
    resp = requests.get('http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=%s' % query)
    data = json.loads(resp.text)
    if data['responseStatus'] == 200:
        return data['responseData']['results']
    return []


def search_place(query):
    resp = requests.get(
        'https://maps.googleapis.com/maps/api/place/textsearch/json?query=%s&key=%s' % (query, BROWSER_API_KEY))
    data = json.loads(resp.text)
    if data['status'] == 'OK':
        lat = data['results'][0]['geometry']['location']['lat']
        lng = data['results'][0]['geometry']['location']['lng']
        address = data['results'][0]['formatted_address']
        name = data['results'][0]['name']
        return Place(lat, lng, address, name)
