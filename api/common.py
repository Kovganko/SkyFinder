import json
import os
import uuid
import io

from django.core.cache import cache
from lxml import html
from requests_futures.sessions import FuturesSession

from app.models import Company
from configuration.settings import BASE_DIR


def generate_string():
    """
    Generate random string value
    :return: str
    """
    return str(uuid.uuid4())


def render_template(name, *args):
    """
    Generate content data for selected location
    :param name: template name
    :param args: anything parameters
    :return: content of html with data params
    """
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates', name), 'r') as f:
        content = f.read()
        return content.format(*args)


def regenerate_content(data):
    """
    Regenerate data with add base template
    :param dict data: dict with content and locations
    """
    for i in data.keys():
        content = ''
        for j in data[i]:
            content += j
        data[i] = render_template('base.html', content)
    return data


class Checker:
    """
    Setup check condition
    """
    instagram_check = False
    vk_check = False
    twitter_check = False
    foursquare_check = False

    @classmethod
    def setup(cls, param):
        if param in ['All', 'all', 'All social']:
            cls.instagram_check = True
            cls.vk_check = True
            cls.twitter_check = True
            cls.foursquare_check = True
        elif param == 'Instagram':
            cls.vk_check = False
            cls.twitter_check = False
            cls.foursquare_check = False
            cls.instagram_check = True
        elif param == 'Vkontakte':
            cls.twitter_check = False
            cls.foursquare_check = False
            cls.instagram_check = False
            cls.vk_check = True
        elif param == 'Twitter':
            cls.vk_check = False
            cls.foursquare_check = False
            cls.instagram_check = False
            cls.twitter_check = True
        elif param == 'Foursquare':
            cls.vk_check = False
            cls.foursquare_check = True
            cls.instagram_check = False
            cls.twitter_check = False
        else:
            pass


class Worker(object):
    """
    Worker for send requests and after parse response
    """

    def __init__(self, root_xpath):
        self.length = 0
        self.jobs = {}
        self.cache_key = None
        self.cache_time = 3600
        self.root_xpath = root_xpath
        self.session = FuturesSession()
        self.req_session = None
        self.sessions = {}
        self.temp = []
        self._company = None
        self.jobs_links = None

    def request(self, *args, **kwargs):
        pass

    def logo(self, element):
        return ''

    def link(self, element):
        return ''

    def text(self, element):
        return ''

    def title(self, element):
        return ''

    def company(self, element):
        return ''

    def created(self, element):
        return ''

    def address(self, element):
        return ''

    def company_link(self, element):
        return ''

    def parse_response(self, _array):
        result = cache.get(self.cache_key)
        if not result:
            result = self.req_session.result().text
            cache.set(self.cache_key, result, self.cache_time)
        doc = html.fromstring(result)
        flag_search = True
        found = doc.xpath('.//*[@class="rua-p-c-black"]')
        if len(found) > 0:
            if found[0].text == '0':
                flag_search = False
        if flag_search:
            self.jobs_links = doc.xpath(self.root_xpath)
            for j in self.jobs_links:
                link = self.link(j)
                if link == '':
                    continue
                company = self.company(j)
                if company == '':
                    continue
                if len(company) > 5:
                    self._company = Company.objects.filter(
                        name__icontains=company.lstrip(' '))
                else:
                    self._company = Company.objects.filter(
                        name=company.lstrip(' '))
                company_link = self.company_link(j)
                getted_logo = self.logo(j)
                logo = getted_logo if getted_logo != '' else \
                    '/media/logo/22017cd6-12a6-4a65-82e1-a20e6563eaa4.png'
                self.jobs[link] = {'href': link,
                                   'title': self.title(j),
                                   'description': self.text(j),
                                   'logo': logo,
                                   'company': company,
                                   'created': self.created(j),
                                   'address': self.address(j),
                                   'location': '',
                                   'company_link': company_link
                                   }
                if not self._company.exists() and company not in self.temp:
                    self.temp.append(company)
                    self.sessions[link] = self.session.get(
                        'http://maps.googleapis.com/maps/api/geocode/'
                        'json?address=%s+kiev&sensor=false' % company)
                elif company in self.temp:
                    pass
                else:
                    company_link = company_link \
                        if self._company[0].link is None else \
                        self._company[0].link
                    self.jobs[link].update(
                        {'location': self._company[0].location,
                         'address': self._company[0].addresses,
                         'company_link': company_link,
                         'logo': self._company[0].logo})

        for i in self.sessions.keys():
            t = json.loads(self.sessions[i].result().text)
            address = ''
            location_str = ''
            if t['status'] == 'OK':
                location = t['results'][0]['geometry']['location']
                location_str = '%s,%s' % (location['lat'], location['lng'])
                address = t['results'][0]['formatted_address']
            self.jobs[i].update({'location': location_str, 'address': address})
            c = Company.objects.create(
                name=self.jobs[i]['company'],
                addresses=address,
                location=location_str,
                logo=self.jobs[i]['logo'],
                link=self.jobs[i]['company_link'],
                description=self.jobs[i]['description'])
            c.save()
        for i in self.jobs.keys():
            location = self.jobs[i]['location']
            try:
                _company = Company.objects.get(name=self.jobs[i]['company'])
            except Exception:
                continue
            content = render_template(
                'work_ua.html', self.jobs[i]['logo'], self.jobs[i]['href'],
                self.jobs[i]['title'], self.jobs[i]['company_link'],
                self.jobs[i]['description'], self.jobs[i]['address'],
                self.jobs[i]['created'], self.jobs[i]['company'],
                _company.rating)
            if location in _array.keys():
                _array[location].append(content)
            else:
                _array[location] = [content]
            self.length += 1
        return self.length


def add_ip_address_to_list(user, ip):
    """
    Add ip address to list in Person model
    :param ip: str, ip address client
    :param user: model of currect user
    """
    ips = user.ip_addresses
    if ips is not None:
        _list = ips.split(',')
        if ip not in _list:
            if len(_list) > 4:
                _list.pop(0)
            content = ''
            _list.append(ip)
            for i in _list:
                content += i + ','
            user.ip_addresses = content[:-1]
    else:
        user.ip_addresses = ip


def compare_strings(str1, str2):
    """
    Compare two string information and return percents of identity
    :param str1: common info
    :param str2: original string, what want compare
    :return: float percent
    """
    str1 = str1.replace('"', '').replace(',', '').replace('/', '')
    str2 = str2.replace('"', '').replace(',', '').replace('/', '')
    s1 = str1.split(' ')
    s2 = str2.split(' ')
    result_set = set(s1) & set(s2)
    return len(result_set) / len(s1) * 100


def get_localization(lang):
    """
    Get string localized data
    :param lang: language
    :return: str data
    """
    with io.open(os.path.join(
            BASE_DIR, 'locale', '%s.json' % lang), 'r', encoding='utf-8') as f:
        t = f.read()
        return json.loads(t)
