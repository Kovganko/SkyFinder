# -*- coding: UTF-8 -*-
from requests_futures.sessions import FuturesSession
from django.core.cache import cache
from api.common import Worker


class WorkUa(Worker):
    def request(self, query, city='Киев'):
        regions = {
            'Киев': 'kyiv'
        }
        page = 1
        query = query.replace(' ', '+')
        self.cache_key = 'http://www.work.ua/jobs-{2}-{0}/?page={1}'.format(
            query, page, regions[city])
        result = cache.get(self.cache_key)
        if not result:
            self.req_session = FuturesSession().get(self.cache_key)

    def logo(self, element):
        div_img = element.xpath('div[1]/img')
        if len(div_img) > 0:
            return div_img[0].attrib.get('src')
        return ''

    def title(self, element):
        h2a = element.xpath('h2/a')[0]
        return str(h2a.text)

    def company(self, element):
        div_span = element.xpath('div[last()]/span[1]')[0]
        return str(div_span.text)

    def created(self, element):
        div_span_last = element.xpath('div[last()]/span[last()]/text()')
        if len(div_span_last) > 1:
            return str(div_span_last[1])
        return ''

    def link(self, element):
        h2a = element.xpath('h2/a')[0]
        return 'http://work.ua%s' % h2a.attrib.get('href')

    def text(self, element):
        p_text = element.xpath('p/text()')
        return str(p_text[0]) + str(p_text[1])


class DouUa(Worker):
    def request(self, query, city='Киев'):
        query = query.replace(' ', '%20').replace('-', '%20')
        self.cache_key = 'http://jobs.dou.ua/vacancies/' \
                         '?city={1}&search={0}'.format(query, city)
        result = cache.get(self.cache_key)
        if not result:
            self.req_session = FuturesSession().get(self.cache_key)

    def title(self, element):
        return str(element.xpath('a/text()')[0])

    def company(self, element):
        return str(element.xpath(
            'strong/a[@class="company"]/text()')[0]).lstrip().rstrip()

    def link(self, element):
        return str(element.xpath('a/@href')[0])

    def text(self, element):
        text = ''
        try:
            text = element.xpath(
                '../div[@class="sh-info"]/text()')[0].replace('\t', '')
        except IndexError:
            if self._company.exists():
                text = self._company[0].description
        return text


class RabotaUa(Worker):
    def request(self, query, city='Киев'):
        regions = {
            'Киев': '1'
        }
        query = query.replace(' ', '+')
        self.cache_key = 'http://rabota.ua/jobsearch/vacancy_list?regionId' \
                         '={1}&keyWords={0}'.format(query, regions[city])
        result = cache.get(self.cache_key)
        if not result:
            self.req_session = FuturesSession().get(self.cache_key)

    def title(self, element):
        return str(element.xpath(
            'div[@class="rua-g-clearfix"]/h3/a/text()')[0])

    def company(self, element):
        company = ''
        x = element.xpath('div[@class="rua-g-clearfix"]/div/text()')
        if len(x) > 0:
            company = str(x[0]).lstrip().rstrip()
        return company

    def link(self, element):
        try:
            return 'http://rabota.ua%s' % str(element.xpath(
                'div[@class="rua-g-clearfix"]/h3/a/@href')[0])
        except IndexError:
            return ''

    def text(self, element):
        return str(element.xpath('div[@class="d"]/text()')[0])

    def created(self, element):
        created = ''
        _created = element.xpath('div[@class="dt"]/text()')
        if len(_created) > 0:
            created = _created[0]
        return created
