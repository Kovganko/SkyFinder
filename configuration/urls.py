"""untitled URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.static import serve

from app import views
from configuration.settings import MEDIA_ROOT, STATIC_ROOT

urlpatterns = [
    url(r'^static/(?P<path>.*)$', serve, {'document_root': STATIC_ROOT}),
    url(r'^$', views.index, name='index'),
    url(r'^index', views.index, name='index'),
    url(r'^home', views.home, name='home'),
    url(r'^remind_username', views.remind_username, name='remind_username'),
    url(r'^remind_password', views.remind_password, name='remind_password'),
    url(r'^change_profile', views.change_profile, name='change_profile'),
    url(r'^send_rating', views.send_rating, name='send_rating'),
    url(r'^find_checkins_friends', views.find_checkins_friends,
        name='find_checkins_friends'),
    url(r'^send_foursquare_info', views.send_foursquare_info,
        name='send_foursquare_info'),
    url(r'^update_settings', views.update_settings, name='update_settings'),
    url(r'^restore_password_form', views.restore_password_form,
        name='restore_password_form'),
    url(r'^reset_password', views.reset_password,
        name='reset_password'),
    url(r'^send_message', views.send_message, name='send_message'),
    url(r'^signin', views.signin, name='signin'),
    url(r'^register', views.register, name='register'),
    url(r'^logout_user', views.logout_user, name='logout_user'),
    url(r'^sitemap\.xml$', views.sitemap, name='sitemap'),
    url(r'^works', views.works, name='works'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': MEDIA_ROOT}),
    url(r'^robots\.txt$', views.robots, name='robots'),
]
